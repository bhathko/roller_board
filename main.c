#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define maxSize 6
#define maxStepSize 50000

int baseBoard[maxSize][maxSize];
int previousBoard[maxSize][maxSize];
int nextBoard[maxSize][maxSize];
int checkedBoard[maxStepSize][maxSize];
int checkBoardIndex = 0;
int stepsIndex = 0;
int nextStepsIndex = 0;
bool notDone = true;

char steps[maxStepSize][50];
char nextSteps[maxStepSize][50];

int count = 0;

void initBoard();
void loadBoard(char *path, int *width, int *height);
bool isNewBoard(int width, int height);
void printBoard(int board[maxSize][maxSize]);
void clearNextSteps();
void clearSteps();


void tryAllPossible(int width, int height, char currentPath[]);
void printPathBoard(char path[], int width, int height);
bool checkBoardIsDone(int width);
int main() {
    int width =0, height = 0;
    initBoard();
    loadBoard("", &width, &height);
    clearNextSteps();
    while (notDone) {
        count++;
        if (nextStepsIndex == 0){
            tryAllPossible(width, height, "");
        } else {
            clearSteps();
            stepsIndex = nextStepsIndex;
            printf("Round %d, stepIndex %d:\n", count, nextStepsIndex);
            for (int j = 0; j < stepsIndex; j++) {
               strcpy(steps[j], nextSteps[j]);
//               printf("%s  index: %d\n", steps[j], stepsIndex);
            }
//            printf("\n\n");
            clearNextSteps();
            nextStepsIndex = 0;
            for (int j = 0; j < stepsIndex; ++j) {
                tryAllPossible(width, height, steps[j]);
            }
        }
    }


    
    return 0;
}

void initBoard() {
    for (int i =0; i < maxSize; i++) {
        for (int j = 0; j < maxSize; j++) {
            baseBoard[i][j] = 0;
            previousBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }

    for (int i =0; i < 2000; i++) {
        for (int j = 0; j < maxSize; j++) {
            checkedBoard[i][j] = 0;
        }
    }

}

void loadBoard(char *path, int *width,int *height) {
    char *line;
    int length = 0;
    int row = 0;

//    FILE *boardFile = fopen(path, "r");
    FILE *boardFile = fopen("../board.txt", "r");

    size_t buffer = 20;
    line = (char*) malloc(buffer * sizeof(char));

    while(length != -1) {
        length = (int)getline(&line, &buffer, boardFile);
        if( length > 0) {
            *width = length;
        }
        for (int i = 0; i < length; i++) {
            if ((int)line[i] == 49) {
                baseBoard[row][i] = 1;
                previousBoard[row][i] = 1;
                nextBoard[row][i] = 1;
            }
        }
        row++;
    }
//    printBoard(baseBoard);
    printf("\n");
    *height = row - 1;
    isNewBoard(*width, *height);
    fclose(boardFile);
    free(line);
};

void resetNextBoard() {
    for (int i = 0; i < maxSize; i++) {
        for (int j = 0; j < maxSize; j++) {
            nextBoard[i][j] = previousBoard[i][j];
        }
    }
}


void rotateTop(int column, int height, int board[maxSize][maxSize]){
    int *tempValue = (int *)malloc(height * sizeof (int));
    for (int i = 0; i < height; i++) {
        tempValue[i] = board[i][column];
    };
    for (int i = 0; i < height; i++) {
        board[(i - 1 + height) % height][column] = tempValue[i];
    };
}


void rotateDown(int column, int height, int board[maxSize][maxSize]){
    int *tempValue = (int *)malloc(height * sizeof (int));
    for (int i = 0; i < height; i++) {
        tempValue[i] = board[i][column];
    };
    for (int i = 0; i < height; i++) {
        board[(i + 1) % height][column] = tempValue[i];
    };
}


void rotateLeft(int row, int width, int board[maxSize][maxSize]){
    int *tempValue = (int *)malloc(width * sizeof (int));
    for (int i = 0; i < width; i++) {
        tempValue[i] = board[row][i];
    };
    for (int i = 0; i < width; i++) {
        board[row][(i - 1 + width) % width] = tempValue[i];
    };
}

void rotateRight(int row, int width, int board[maxSize][maxSize]){
    int *tempValue = (int *)malloc(width * sizeof (int));
    for (int i = 0; i < width; i++) {
        tempValue[i] = board[row][i];
    };
    for (int i = 0; i < width; i++) {
        board[row][(i + 1) % width] = tempValue[i];
    };
}

int encoder(int numList[], int listSize) {
    int num = 0;
    for (int i = 0; i < listSize; i++) {
        if (numList[i] == 1) {
            int pow = 1;
            for (int j = i ; j > 0; j--) {
                pow *= 2;
            }
            num += pow;
        }
    }
    return num;
}

void setPreviousBoard(char path[], int width, int height) {
//    printf("%d\n", count++);
    for (int i = 0; i < maxSize; i++) {
        for (int j = 0; j < maxSize; j++) {
            previousBoard[i][j] = baseBoard[i][j];
        }
    }
//    printBoard(previousBoard);
    if (strlen(path) >= 2) {
        for (int i = 0; i < strlen(path); i+=2) {
            switch (path[i]) {
                case 'T':
                    rotateTop((int)path[i+1] - 48, height, previousBoard);
                    break;
                case 'D':
                    rotateDown((int)path[i+1] - 48, height, previousBoard);
                    break;
                case 'L':
                    rotateLeft((int)path[i+1] - 48, width, previousBoard);
                    break;
                case 'R':
                    rotateRight((int)path[i+1] - 48, width, previousBoard);
                    break;
            }
        }
    }
    if (checkBoardIsDone(width)) {
        printf("%s\n", path);
        printPathBoard(path, width, height);
        exit(0);
    };
}


bool isNewBoard(int width, int height) {
    int *tempValue = (int *) malloc(width * sizeof (int));
    bool result = true;
    for (int i = 0; i < height; i++) {
        tempValue[i] = encoder(nextBoard[i], width);
//        printf("%d", tempValue[i]);
    }
    for (int i = 0; i < checkBoardIndex; i++) {
        bool matchAll = true;
        for (int j = 0; j < width; ++j) {
            if (tempValue[j] != checkedBoard[i][j]) {
                matchAll = false;
            }
        }
        if (matchAll) {
            result = false;
            i  = checkBoardIndex;
            break;
        }
    }

    if (result) {
        for (int i = 0; i < width; i++) {
            checkedBoard[checkBoardIndex][i] = tempValue[i];
        }
        checkBoardIndex++;
    }
    return result;
}

void tryAllPossible(int width, int height, char currentPath[]) {
//    unsigned long s = strlen(currentPath);
//    printf("%lu", s);
    setPreviousBoard(currentPath, width, height);
    char action[100];
    for (int i = 0; i < height; i++) {
        resetNextBoard();
        rotateTop(i, height, nextBoard);
        if (isNewBoard(width, height)) {
            sprintf(action, "T%d", i);
            strcat(nextSteps[nextStepsIndex], currentPath);
            strcat(nextSteps[nextStepsIndex], action);
            nextStepsIndex++;
        }
        resetNextBoard();
        rotateDown(i, height, nextBoard);
        if (isNewBoard(width, height)) {
            sprintf(action, "D%d", i);
            strcat(nextSteps[nextStepsIndex], currentPath);
            strcat(nextSteps[nextStepsIndex], action);
            action[0] = '\0';
            nextStepsIndex++;
        }
    }

    for (int i = 0; i < width; i++) {
        resetNextBoard();
        rotateLeft(i, width, nextBoard);
        if (isNewBoard(width, height)) {
            sprintf(action, "L%d", i);
            strcat(nextSteps[nextStepsIndex], currentPath);
            strcat(nextSteps[nextStepsIndex], action);
            nextStepsIndex++;
        }
        resetNextBoard();
        rotateRight(i, width, nextBoard);
        if (isNewBoard(width, height)) {
            sprintf(action, "R%d", i);
            strcat(nextSteps[nextStepsIndex], currentPath);
            strcat(nextSteps[nextStepsIndex], action);
            nextStepsIndex++;
        }
    }
}

void printBoard(int board[maxSize][maxSize]) {
    for (int i = 0; i < maxSize; i++) {
        for (int j = 0; j < maxSize; j++) {
            printf("%d", board[i][j]);
        }
        printf("\n");
    }
}

void clearNextSteps() {
    for (int i = 0; i < maxStepSize; i++) {
        for (int j = 0; j < 50; j++) {
            nextSteps[i][j] = '\0';
        }
    }
}
void clearSteps() {
    for (int i = 0; i < maxStepSize; i++) {
        for (int j = 0; j < 50; j++) {
            steps[i][j] = '\0';
        }
    }
}

bool checkBoardIsDone(int width) {
    bool isDone = true;
    for (int i = 0; i < width; i++) {
        if (previousBoard[0][i] != 1) {
            isDone = false;
        }
    }
    if (isDone) {
//        printBoard(baseBoard);
//        printf("\n");
//        printBoard(previousBoard);
        notDone = false;
    }
    return isDone;
}

void printPathBoard(char path[], int width, int height) {
    for (int i = 0; i < maxSize; i++) {
        for (int j = 0; j < maxSize; j++) {
            previousBoard[i][j] = baseBoard[i][j];
        }
    }
    for (int i = 0; i < strlen(path); i+=2) {
        printf("Case %d\n", (i /2) + 1);
        switch (path[i]) {
            case 'T':
                rotateTop((int)path[i+1] - 48, height, previousBoard);
                printBoard(previousBoard);
                break;
            case 'D':
                rotateDown((int)path[i+1] - 48, height, previousBoard);
                printBoard(previousBoard);
                break;
            case 'L':
                rotateLeft((int)path[i+1] - 48, width, previousBoard);
                printBoard(previousBoard);
                break;
            case 'R':
                rotateRight((int)path[i+1] - 48, width, previousBoard);
                printBoard(previousBoard);
                break;
        }
    }
}